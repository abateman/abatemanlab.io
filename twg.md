---
layout: page
title: TWG architecture
permalink: /twg/
---

# TWG Wales Project

## Architecture

[Architecture diagram (PDF)](/files/TWG_architecture_v3.0.0.pdf)

## What a Welsh dragon looks like

![Dragon](/files/Dragon_Small_Red_RGB.jpg)

## General development process

- Pull changes
- Run `composer install` from repo root
- From `/docroot/themes/custom/patternlab_base` dir, run:
  - `npm install; npm run setup; npm run build:drupal; npm run build:drupal --theme=twg_wales`
  - This can be from guest or host, is faster on host.
- Run `drush cim` from VM guest (`vagrant ssh twg-wales`)


